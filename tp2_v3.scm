#! /usr/bin/env gsi -:dR
;;; Fichier : tp2.scm

;;; Ce programme est une version incomplete du TP2.  Vous devez uniquement
;;; changer et ajouter du code dans la premi�re section.

;;;----------------------------------------------------------------------------

;;; Vous devez modifier cette section.  La fonction "traiter" doit
;;; �tre d�finie, et vous pouvez ajouter des d�finitions de fonction
;;; afin de bien d�composer le traitement � faire en petites
;;; fonctions.  Il faut vous limiter au sous-ensemble *fonctionnel* de
;;; Scheme dans votre codage (donc n'utilisez pas set!, set-car!,
;;; begin, etc).

;;; La fonction traiter re�oit en param�tre une liste de caract�res
;;; contenant la requ�te lue et le dictionnaire des variables sous
;;; forme d'une liste d'association.  La fonction retourne
;;; une paire contenant la liste de caract�res qui sera imprim�e comme
;;; r�sultat de l'expression entr�e et le nouveau dictionnaire.  Vos
;;; fonctions ne doivent pas faire d'affichage car c'est la fonction
;;; "repl" qui se charge de cela.

(define traiter
  (lambda (expr dict)
    (let ((lecture (lire-expr expr dict)))
        (cons (append ;(string->list "*** le programme est ")
                    ;'(#\I #\N #\C #\O #\M #\P #\L #\E #\T #\! #\newline)
                    ;(string->list "*** la requete lue est: ")
                    ;expr
                    ;(string->list "\n*** nombre de caract�res: ")
                    ;(string->list (number->string (length expr)))
                    ;(string->list "\n*** resultat de l'operation: ")
                    (car lecture)
                    '(#\newline))
            (cdr lecture)))))

;;;----------------------------------------------------------------------------

;;; Ne pas modifier cette section.

(define repl
  (lambda (dict)
    (print "# ")
    (let ((ligne (read-line)))
      (if (string? ligne)
          (let ((r (traiter-ligne ligne dict)))
            (for-each write-char (car r))
            (repl (cdr r)))))))

(define traiter-ligne
  (lambda (ligne dict)
    (traiter (string->list ligne) dict)))

(define main
  (lambda ()
    (repl '()))) ;; dictionnaire initial est vide
    
;;;----------------------------------------------------------------------------

(define lire-expr-aux
    (lambda (expr dict tmp pile)
        (cond ((null? expr) ;;Fin de l'expression
                    (if (null? pile) (string->list tmp) ;;Cas o� seulement un chiffre est entr�
                                     (cons (string->list (number->string (car pile))) dict))) ;; Retourner contenu de la pile
               
              ((eq? (car expr) #\space)
                (lire-expr-aux (cdr expr) dict
                                "" ;;Vider tmp pour r�cup�rer le prochain nombre
                                (if (> (string-length tmp) 0) 
                                    (append (list (string->number tmp)) pile) ;;Si on a fini de lire un nombre, le mettre sur la pile
                                    pile))) 
              
              ((eq? (car expr) #\+) ;; Addition
                (lire-expr-aux (cdr expr) dict
                                ""
                                (operation + pile)))
                                
              ((eq? (car expr) #\-) ;; Soustraction
                (lire-expr-aux (cdr expr) dict
                                ""
                                (operation - pile)))
                                
              ((eq? (car expr) #\*) ;; Multiplication
                (lire-expr-aux (cdr expr) dict
                                ""
                                (operation * pile)))
           
              ((char-numeric? (car expr)) ;; Lecture d'un chiffre
                (lire-expr-aux (cdr expr) dict
                                (string-append tmp (string (car expr))) ;; Ajout au nombre en cours de lecture
                                pile))

	      ((char-alphabetic? (car expr)) ;; Lecture d'une variable
	       (if (null? dict)
		   (string->list (string-append (string (car expr)) " n'a pas de valeur car le dictionnaire est vide"))
		   (let ((variable (assoc (car expr) dict)))
		     (if (pair? variable)
		      (lire-expr-aux (cdr expr) dict
		   		    ""
		   		    (append (list (cdr variable)) pile)) ;; Ajout la valeur de variable lu dans dictionnair au nombre en cours de lecture
		   (string->list (string-append (string (car expr)) " n'a pas de valeur"))))))

	      ((eq? (car expr) #\=) ;; Affectation
	       (cond ((null? pile)
		   (string->list "Vous devriez entrer une valeur avant d'affectation"))
		     ((or (not (char-alphabetic? (cadr expr))) (null? (cdr expr)))
		      (string->list "Vous devriez entrer un nom de variable sans espace apres ="))
		   (else (lire-expr-aux (cddr expr) (affectation expr dict pile)
			       ""
			       pile))))
              (else tmp))))

(define lire-expr
 (lambda (expr dict)
   (lire-expr-aux expr dict "" '())))
   
   
(define operation
    (lambda(op pile)  ;;Appel a la proc�dure op sur les deux premiers �l�ments de la pile     
        (append (list (op (cadr pile) (car pile))) (cddr pile)))) ;;Remplace les op�randes par le r�sultat

(define affectation ;; ajouter la variable et sa valeur au dictionnaire dans les cas ou le dict est vide ou la variable n'existe pas dans le dict
  (lambda (expr dict pile)
    (if (not (pair? (assoc (cadr expr) dict)))
	(append (list (cons (cadr expr) (car pile))) dict)
	(creer_dict (cadr expr) (car pile) dict))))

(define creer_dict ;; creer une nouveau dictionnair lorsque la variable ajoute est deja dans le dict
  (lambda (cle val lst)
    (if (null? lst)
	(list (cons cle val))
	(if (eq? cle (caar lst))
	    (creer_dict cle val (cdr lst))
	    (append (list (cons (caar lst) (cdar lst)))
		     (creer_dict cle val (cdr lst)))))))

;; (define creer_dict2
;;   (lambda ())
;;   (affectation expr))


;; pour tester
;; (define expr '(#\5 #\0 #\space #\1 #\2 #\space #\+))
;; (define expr '(#\5 #\0 #\space #\= #\a))
;; (define dict '((a . 10) (b . 20) (d . 40)))
