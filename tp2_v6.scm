#! /usr/bin/env gsi -:dR
;;; Fichier : tp2.scm

;;; Ce programme est une version incomplete du TP2.  Vous devez uniquement
;;; changer et ajouter du code dans la premi�re section.

;;;----------------------------------------------------------------------------

;;; Vous devez modifier cette section.  La fonction "traiter" doit
;;; �tre d�finie, et vous pouvez ajouter des d�finitions de fonction
;;; afin de bien d�composer le traitement � faire en petites
;;; fonctions.  Il faut vous limiter au sous-ensemble *fonctionnel* de
;;; Scheme dans votre codage (donc n'utilisez pas set!, set-car!,
;;; begin, etc).

;;; La fonction traiter re�oit en param�tre une liste de caract�res
;;; contenant la requ�te lue et le dictionnaire des variables sous
;;; forme d'une liste d'association.  La fonction retourne
;;; une paire contenant la liste de caract�res qui sera imprim�e comme
;;; r�sultat de l'expression entr�e et le nouveau dictionnaire.  Vos
;;; fonctions ne doivent pas faire d'affichage car c'est la fonction
;;; "repl" qui se charge de cela.

(define traiter
  (lambda (expr dict)
    (let ((lecture (lire-expr expr dict)))
        (cons (append ;(string->list "*** le programme est ")
                    ;'(#\I #\N #\C #\O #\M #\P #\L #\E #\T #\! #\newline)
                    ;(string->list "*** la requete lue est: ")
                    ;expr
                    ;(string->list "\n*** nombre de caract�res: ")
                    ;(string->list (number->string (length expr)))
                    ;(string->list "\n*** resultat de l'operation: ")
                    (car lecture)
		    '(#\newline))
            (cdr lecture)))))

;;;----------------------------------------------------------------------------

;;; Ne pas modifier cette section.

(define repl
  (lambda (dict)
    (print "# ")
    (let ((ligne (read-line)))
      (if (string? ligne)
          (let ((r (traiter-ligne ligne dict)))
            (for-each write-char (car r))
            (repl (cdr r)))))))

(define traiter-ligne
  (lambda (ligne dict)
    (traiter (string->list ligne) dict)))

(define main
  (lambda ()
    (repl '()))) ;; dictionnaire initial est vide
    
;;;----------------------------------------------------------------------------

(define lire-expr-aux
    (lambda (expr dict_global dict_local tmp pile)
        (cond ((null? expr) ;;Fin de l'expression
                    (if (null? pile) (cons (string->list tmp) dict_local) ;;Cas o� seulement un chiffre est entr�
                        (if (> (length pile) 1)
			    (cons (exception 'peu_operation expr) dict_global)
			    (cons (string->list (number->string (car pile))) dict_local)))) ;; Retourner contenu de la pile
               
              ((eq? (car expr) #\space)
                (lire-expr-aux (cdr expr) dict_global dict_local
                                "" ;;Vider tmp pour r�cup�rer le prochain nombre
                                (if (> (string-length tmp) 0) 
                                    (append (list (string->number tmp)) pile) ;;Si on a fini de lire un nombre, le mettre sur la pile
                                    pile))) 
              
              ((eq? (car expr) #\+) ;; Addition
	       (if (< (length pile) 2)
		   (cons (exception 'peu_operand expr) dict_global)
		   (lire-expr-aux (cdr expr) dict_global dict_local
				  ""
				  (operation + pile))))
                                
              ((eq? (car expr) #\-) ;; Soustraction
	       (if (< (length pile) 2)
                   (cons (exception 'peu_operand expr) dict_global)
		   (lire-expr-aux (cdr expr) dict_global dict_local
				  ""
				  (operation - pile))))
                                
              ((eq? (car expr) #\*) ;; Multiplication
	       (if (< (length pile) 2)
		   (cons (exception 'peu_operand expr) dict_global)             
		   (lire-expr-aux (cdr expr) dict_global dict_local
				  ""
				  (operation * pile))))
           
              ((char-numeric? (car expr)) ;; Lecture d'un chiffre
                (lire-expr-aux (cdr expr) dict_global dict_local
                                (string-append tmp (string (car expr))) ;; Ajout au nombre en cours de lecture
                                pile))

	      ((char-alphabetic? (car expr)) ;; Lecture d'une variable
	       (if (null? dict_local)
                   (cons (exception 'dict_vide expr) dict_global)
		   (let ((variable (assoc (car expr) dict_local)))
		     (if (pair? variable)
		      (lire-expr-aux (cdr expr) dict_global dict_local
		   		    ""
		   		    (append (list (cdr variable)) pile)) ;; Ajout la valeur de variable lu dans dictionnair au nombre en cours de lecture
		      (cons (exception 'paire_introuv expr) dict_global)))))

	      ((eq? (car expr) #\=) ;; Affectation
	       (cond ((null? pile)
		   (cons (exception 'affect_pile_vide expr) dict_global))
		     ((or (not (char-alphabetic? (cadr expr))) (null? (cdr expr)))
		      (cons (exception 'affect_sans_nom expr) dict_global))
		     (else (lire-expr-aux (cddr expr) dict_global (affectation expr dict_local pile)
			       ""
			       pile))))
	      
	      ((and (eq? (car expr) #\,) (eq? (cadr expr) #\q)) ;; quitter la calculatrice
	       (exit))
	      
              (else tmp))))

(define lire-expr
 (lambda (expr dict)
   (lire-expr-aux expr dict dict "" '())))
   
(define operation
    (lambda(op pile)  ;;Appel a la proc�dure op sur les deux premiers �l�ments de la pile     
        (append (list (op (cadr pile) (car pile))) (cddr pile)))) ;;Remplace les op�randes par le r�sultat

(define affectation ;; ajouter la variable et sa valeur au dictionnaire dans les cas ou le dict est vide ou la variable n'existe pas dans le dict
  (lambda (expr dict pile)
    (if (not (pair? (assoc (cadr expr) dict)))
	(append (list (cons (cadr expr) (car pile))) dict)
	(creer-dict (cadr expr) (car pile) dict))))

(define creer-dict-aux ;; pour rendre creer-dict iterative
  (lambda (cle val old_dict new_dict)
    (if (null? old_dict)
	(append (list (cons cle val)) new_dict)
	(if (eq? cle (caar old_dict))
	    (creer-dict-aux cle val (cdr old_dict) new_dict)
	    (creer-dict-aux cle val (cdr old_dict) (append (list (cons (caar old_dict) (cdar old_dict))) new_dict))))))

(define creer-dict ;; creer une nouveau dictionnair lorsque la variable ajoute est deja dans le dict ou le dict est vide
  (lambda (cle val old_dict)
    (creer-dict-aux cle val old_dict '())))


(define exception
(lambda (msg expr)
  (cond
   ((equal? msg 'peu_operation)
    (string->list "pas assez d'operation a effectuer"))
   ((equal? msg 'peu_operand)
    (string->list "Vous devriez entrer au moins deux valeurs pour faire une operation ou il y a plus d'operation que d'oerand"))
   ((equal? msg 'dict_vide)
    (string->list (string-append (string (car expr)) " n'a pas de valeur car le dictionnaire est vide")))
   ((equal? msg 'paire_introuv)
    (string->list (string-append (string (car expr)) " n'existe pas dans le dictionnaire")))
   ((equal? msg 'affect_pile_vide)
    (string->list "Vous devriez entrer une valeur avant d'affectation"))
   ((equal? msg 'affect_sans_nom)
    (string->list "Vous devriez entrer un nom de variable sans espace apres =")))))
